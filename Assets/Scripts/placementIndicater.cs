﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.UI;
using TMPro;
public class placementIndicater : MonoBehaviour
{

    public ARRaycastManager raymanager;
    public GameObject PointerObj;
    public GameObject pointPrefab;
    public LineRenderer measureLine;
    private GameObject startPoint;
    private GameObject endPoint;
    public TextMeshPro distanceText;

    // Start is called before the first frame update
    void Start()
    {
        raymanager = FindObjectOfType<ARRaycastManager>();
        // measureLine.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        List<ARRaycastHit> hitpoint = new List<ARRaycastHit>();
        raymanager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hitpoint, TrackableType.Planes);
        if (hitpoint.Count > 0)
        {
            transform.position = hitpoint[0].pose.position;
            transform.rotation = hitpoint[0].pose.rotation;
            if (!PointerObj.activeInHierarchy)
            {
                PointerObj.SetActive(true);
            }
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                if (touch.phase == TouchPhase.Began)
                    startPoint = Instantiate(pointPrefab, transform.position, Quaternion.identity);
                if (touch.phase == TouchPhase.Moved)
                {
                    endPoint = Instantiate(pointPrefab, transform.position, Quaternion.identity);
                    measureLine.gameObject.SetActive(true);
                }
            }


        }
        if (startPoint.activeSelf && endPoint.activeSelf)
        {
            distanceText.transform.position = ((startPoint.transform.position + endPoint.transform.position) / 2.0f);
            distanceText.transform.rotation = endPoint.transform.rotation;
            measureLine.SetPosition(0, startPoint.transform.position);
            measureLine.SetPosition(1, endPoint.transform.position);

            distanceText.text = $"Distance: {(Vector3.Distance(startPoint.transform.position, endPoint.transform.position)).ToString("F2")} m";
        }



    }


}
