﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using TMPro;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;


public class MeasurementController : MonoBehaviour
{
    [SerializeField]
    private GameObject placementIndicator;
    [SerializeField]
    private GameObject measurementPointPrefab;

    [SerializeField]
    private float measurementFactor = 39.37f;

    [SerializeField]
    private Vector3 offsetMeasurement = Vector3.zero;

    // private GameObject welcomePanel;
    public Text unitname;
    public float[] factor;
    public string[] unit_name;

    public Color32[] color_name;
    // private Button dismissButton;
    int i, j = 0;
    [SerializeField]
    private Text distanceTextUI;
    private TextMeshPro distanceText;
    private TextMeshPro distanceText1;
    [SerializeField]
    private ARCameraManager arCameraManager;
    [SerializeField]
    private LineRenderer measureLine;
    [SerializeField]
    private ARRaycastManager arRaycastManager;
    [SerializeField]
    private GameObject parentObject, text_touch;
    private GameObject startPoint;

    [SerializeField]
    private GameObject endPoint;
    private GameObject text_object;
    [SerializeField]
    private GameObject linedistance;

    private GameObject linedistance1;

    private Vector2 touchPosition = default;

    private bool isplaced = false;

    public string unit = "m";
    private LineRenderer usedLine;
    private static List<ARRaycastHit> hitpoint = new List<ARRaycastHit>();

    public Image colorImage;



    void Update()
    {
        arRaycastManager.Raycast(new Vector2(Screen.width / 2, Screen.height / 2), hitpoint, TrackableType.Planes);
        if (hitpoint.Count > 0)
        {
            text_touch.SetActive(false);
            transform.position = hitpoint[0].pose.position;
            transform.rotation = hitpoint[0].pose.rotation;
            if (!placementIndicator.activeInHierarchy)
            {
                placementIndicator.SetActive(true);
            }
        }
        if (isplaced)
        {
            endPoint.transform.position = transform.position;
            usedLine.SetPosition(0, startPoint.transform.position);
            usedLine.SetPosition(1, transform.position);
            linedistance1.transform.position = (startPoint.transform.position + endPoint.transform.position) / 2f;
            // linedistance1.transform.rotation = Quaternion.Euler(90, 0, transform.rotation.z);
            distanceText1.text = $"{(Vector3.Distance(startPoint.transform.position, endPoint.transform.position) * measurementFactor).ToString("F2")}" + unit;
            distanceTextUI.text = $"{(Vector3.Distance(startPoint.transform.position, endPoint.transform.position) * measurementFactor).ToString("F2")}" + unit;
        }
    }

    public void placePoint()
    {
        if (!isplaced)
        {

            startPoint = Instantiate(measurementPointPrefab, transform.position, Quaternion.identity);
            endPoint = Instantiate(measurementPointPrefab, transform.position, Quaternion.identity);
            isplaced = true;
            usedLine = Instantiate(measureLine);
            linedistance1 = Instantiate(linedistance);
            linedistance1.SetActive(true);
            linedistance1.transform.position = (startPoint.transform.position + transform.position) / 2f;
            text_object = linedistance1.transform.GetChild(0).gameObject;
            distanceText1 = text_object.GetComponent<TextMeshPro>();
            startPoint.transform.parent = parentObject.transform;
            usedLine.transform.parent = parentObject.transform;
            linedistance1.transform.parent = parentObject.transform;

        }
        else
        {
            //endPoint = Instantiate(measurementPointPrefab, transform.position, Quaternion.identity);
            usedLine.SetPosition(1, endPoint.transform.position);
            isplaced = false;
            linedistance1.transform.position = (startPoint.transform.position + endPoint.transform.position) / 2f;
            endPoint.transform.parent = parentObject.transform;

        }

    }

    public void Delete()
    {
        foreach (Transform child in parentObject.transform)
        {
            Destroy(child.gameObject);

        }
        isplaced = false;
    }

    /*  public void unitMeter()
      {
          measurementFactor = 1.0f;
          unit = "m";
      }
      public void unitinch()
      {
          measurementFactor = 39.38f;
          unit = "in";
      }
      public void unitcm()
      {
          measurementFactor = 100.0f;
          unit = "cm";
      }
      public void unitfoot()
      {
          measurementFactor = 3.281f;
          unit = "foot";
      }*/

    public void Color_change(int j)
    {
        measureLine.material.color = color_name[j];
        // colorImage.color = color_name[j]; new Color32(255, 255, 225, 100);
    }

    public void changeImagej()
    {
        if (j < 3)
        {
            j++;
        }
        if (j == 3 || j < 0)
        {
            j = 0;
        }
        Color_change(j);

    }

    /*public void Color_change_yellow()
    {
        measureLine.material = yellow;
    }
    public void Color_change_black()
    {
        measureLine.material = black;
    }
    public void Color_change_green()
    {
        measureLine.material = green;
    }
    public void Color_change_indigo()
    {
        measureLine.material = indigo;
    }
*/
    public void ChangeUnit(int i)
    {
        unit = unit_name[i];
        measurementFactor = factor[i];
        unitname.text = unit;
    }


    public void changei()
    {
        if (i < 3)
        {
            i++;
        }
        if (i == 3 || i < 0)
        {
            i = 0;
        }
        ChangeUnit(i);

    }
}
